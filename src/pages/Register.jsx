import { useState, useContext } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register() {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobile, setMobile] = useState('');
    const [password, setPassword] = useState('');
    const [password2, setPassword2] = useState('');

    const { cUser, setCUser } = useContext(UserContext);

    const navigate = useNavigate();

    const handleLoginEnable = () => {
        if (email !== '' && password !== '' && password2 !== '') return (
            <Button variant="success" type="submit">
                Sign Up
            </Button>
        )

        return (
            <Button variant="success" type="submit" disabled>
                Sign Up
            </Button>
        )
    }

  const handleLogin = () => {
    alert('You have successfully logged in!');
  }

  const checkMobileNo = (mobile) => {
    const mobileRegexIntl = /^([+]\d{2})?\d{10}$/
    const mobileRegex= /\d{11,}$/

    if (mobileRegex.test(mobile) || mobileRegexIntl.test(mobile)) return true;

    return false;
  }

  const checkEmail = () => {
    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: "POST",
      headers: {"Content-type": "application/json"},
      body: JSON.stringify({
        email: email
      })
    }).then(res => res.json())
    .then(data => {
      console.log(data)
      return(data)
    })
  }

  // const retrieveUserDetails = (token) => {
  //   fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
  //     headers: {
  //       Authorization: `Bearer ${token}`
  //     }
  //   })
  //   .then(res => res.json())
  //   .then(data => {
  //     console.log(data);

  //     setCUser({
  //       id: data._id,
  //       isAdmin: data.isAdmin,
  //     })
  //   })
  // }

  const handleRegister = (ev) => {
    ev.preventDefault();

    if (password === password2 && checkMobileNo(mobile) && !checkEmail()) {
      fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
        method: "POST",
        headers: {"Content-type": "application/json"},
        body: JSON.stringify({
          firstName: firstName,
          lastName: lastName,
          email: email,
          mobile: mobile,
          password: password
        })
      })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        console.log(data.accessToken);
        
  
        if (typeof data !== undefined) {
          // localStorage.setItem('token', data.accessToken);
          // retrieveUserDetails(data.accessToken);
          Swal.fire({
              title: "Sign Up Successful",
              icon: "success",
              text: "Log In to Access your Account"
            })
        } else {
          Swal.fire({
            title: "Registration failed",
            icon: "error",
            text: "Check your sign up details"
          })
        }
        
        
      })
    }

    setFirstName('');
    setLastName('');
    setEmail('');
    setMobile('');
    setPassword('');
    setPassword2('');

    navigate('/login');
  }

  return (
    <>
        <h1>Register</h1>
        {/* {(cUser.id !== null) ?
          <Navigate to="/courses"/> : */}
          <Form onSubmit={(ev) => handleRegister(ev)}>
              <Form.Group className="mb-3" controlId="formBasicFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control required type="text" placeholder="Enter first name" value={firstName} onChange={(ev) => {setFirstName(ev.target.value)}}/>
              </Form.Group>
              
              <Form.Group className="mb-3" controlId="formBasicLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control required type="text" placeholder="Enter last name" value={lastName} onChange={(ev) => {setLastName(ev.target.value)}}/>
              </Form.Group>
              
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control required type="email" placeholder="Enter email" value={email} onChange={(ev) => {setEmail(ev.target.value)}}/>
              </Form.Group>
              
              <Form.Group className="mb-3" controlId="formBasicMobile">
                <Form.Label>Mobile No.</Form.Label>
                <Form.Control required type="text" placeholder="Enter mobile number" value={mobile} onChange={(ev) => {setMobile(ev.target.value)}}/>
              </Form.Group>
          
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control required type="password" placeholder="Password" value={password} onChange={(ev) => {setPassword(ev.target.value)}}/>
              </Form.Group>
          
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Confirm Password</Form.Label>
                <Form.Control required type="password" placeholder="Password" value={password2} onChange={(ev) => {setPassword2(ev.target.value)}}/>
              </Form.Group>
          
              <Form.Text>
          
              </Form.Text>
          
              {handleLoginEnable()}
              {/* <Button variant="primary" type="submit">
                Submit
              </Button> */}
            </Form>
        {/* } */}
    </>
  );
}