import './App.css';
import AppNavbar from './components/AppNavbar';

import { Container } from 'react-bootstrap';
import Homepage from './pages/Homepage';
import Courses from './pages/Courses';
import Login from './pages/Login';
import Register from './pages/Register';
import NotFound from './pages/NotFound';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import { useEffect, useState, useContext } from 'react';
import { UserProvider } from './UserContext';
import Settings from './pages/Settings';

function App() {

  //1
  const [cUser, setCUser] = useState({
    email: localStorage.getItem('email')
  })

  const unsetCUser = () => {
    localStorage.removeItem('email');
  }

  const [user, setUser] = useState(localStorage.getItem('email'));

  useEffect(() => {
    setUser(localStorage.getItem('email'));
  },[localStorage.getItem('email')])

  return (
    <UserProvider value={{cUser, setCUser, unsetCUser}}>
      <Router>
        <Container>
          <AppNavbar user={user} setUser={setUser}/>
          
          <Routes>
            <Route path="/" element={<Homepage />}/>
            <Route path="/courses" element={<Courses />}/>
            <Route path="/register" element={<Register />}/>
            <Route path="/login"  element={<Login setUser={setUser}/>}/>
            <Route path="/settings" element={<Settings />}/>
            <Route path="/*" element={<NotFound />}/>
          </Routes>
        </Container>
      </Router>
    </UserProvider>
    
  );
}

export default App;
