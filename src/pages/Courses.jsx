import React, { useEffect } from "react";
import coursesData from "../data/coursesData"
import CourseCard from "../components/CourseCard";
import {Container} from 'react-bootstrap';
import { useState } from "react";

export default function Courses() {
    console.log("Contents of CoursesData: ")
    console.log(coursesData);

    const [courses, setCourses] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setCourses(data)})
    },[])

    return(
        <Container>
            <h1>Courses</h1>
            <Container>
                {courses.map(course => <CourseCard key={course.id} course={course}/>)}
            </Container>
        </Container>
    )
}