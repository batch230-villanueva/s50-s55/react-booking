import UserContext from "../UserContext"
import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { Navigate } from "react-router-dom";

export default function Settings() {

    const { cUser, setCUser } = useContext(UserContext);

    const navigate = useNavigate();

    return(
        <>
            {
            (cUser.email !== null) ?
            <h1>Settings Page</h1> : 
            <Navigate to="/login" />
            }
        </>
    )
}