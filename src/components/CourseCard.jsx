
import {Button, Card} from 'react-bootstrap';
import { useEffect, useState } from 'react';
import PropTypes from 'prop-types'

export default function CourseCard({course}) {

  const {name, description, price, onOffer} = course;

  const [count, setCount] = useState(0);
  const [slots, setSlots] = useState(30);

  const handleButtonEnabled = () => {
    if (onOffer) return <Button variant="primary" onClick={enroll}>Enroll</Button>
    return <Button variant="primary" disabled>Enroll</Button>
  }

  const enroll = () => {
    if (slots) {
        setCount(count + 1);
        setSlots(slots - 1);
    } else {
        alert(`No more slots available for ${name}!`)
    }

    // setCount(count + 1);
    // setSlots(slots - 1);
  }

//   useEffect(() => {
//     if(slots === 0){
//         alert(`No more slots available for ${name}!`);
//     }
//   }, [slots])

  return (
    <Card className='mb-3'>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price</Card.Subtitle>
        <Card.Text>PHP {price}</Card.Text>
        <Card.Subtitle>Enrollees</Card.Subtitle>
        <Card.Text>{count}</Card.Text>
        {handleButtonEnabled()}
      </Card.Body>
    </Card>
  );
}

CourseCard.propTypes = {
    course: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
    })
}