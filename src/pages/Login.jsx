import { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useNavigate } from 'react-router-dom';
import { Navigate } from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login({setUser}) {
    //2
    const { cUser, setCUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isEnabled, setIsEnabled] = useState(false);

    const navigate = useNavigate();

  useEffect(() => {
    if (email !== '' && password !== '') {
        setIsEnabled(true)
    } else {
        setIsEnabled(false)
    }
  }, [email, password])

  // const handleLogin = (ev) => {
  //   ev.preventDefault();

  //   localStorage.setItem('email', email);

  //   setUser(localStorage.getItem('email'))

  //   setCUser({
  //     email: localStorage.getItem('email')
  //   })

  //   setEmail('');
  //   setPassword('');

  //   alert('You have successfully logged in!');
  //   navigate('/');
  // }

  const handleLogin = (ev) => {
    ev.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {"Content-type": "application/json"},
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      console.log(data.accessToken);
      

      if (typeof data !== undefined) {
        localStorage.setItem('token', data.accessToken);
        retrieveUserDetails(data.accessToken);
        Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to Zuitt!"
          })
      } else {
        Swal.fire({
          title: "Authentication failed",
          icon: "error",
          text: "Check your login details"
        })
      }
      
      
    })

    setEmail('');
    setPassword('');

    navigate('/');
  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      setCUser({
        id: data._id,
        isAdmin: data.isAdmin,
      })
    })
  }

  // const retrieveUserDetails = (token) => {
  //     fetch('http://localhost:4000/users/details', {
  //         headers: {
  //             Authorization: `Bearer ${token}`
  //         }
  //     })
  //     .then(res => res.json())
  //     .then(data => {
  //         console.log(data);

  //         setUser({
  //             id: data._id,
  //             isAdmin: data.isAdmin
  //         })
  //     })
  // }

  return (
    <>
         
        <h1>Log In</h1>
        {/* (cUser.email !== null) ? <Navigate to="/courses"/> : */}
        {/* (cUser.email !== null) ? <Navigate to="/courses"/> : */}
        {(cUser.id !== null) ? 
          <Navigate to="/courses"/> :
          <Form onSubmit={ev => handleLogin(ev)}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="Enter email" value={email} onChange={(ev) => {setEmail(ev.target.value)}}/>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" value={password} onChange={(ev) => {setPassword(ev.target.value)}}/>
          </Form.Group>

          <Button variant="success" type="submit" disabled={!isEnabled}>
                Log In
          </Button>

        </Form>
        }
    </>
  );
}