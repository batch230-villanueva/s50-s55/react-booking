import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

import 'bootstrap/dist/css/bootstrap.min.css';

/*
  option 1 preparations:
  - npx create-react-app appName
  - npm start
  - npm install react-bootstrap bootstrap

  option 2 preparations
  - npm install -g create-react-app
  - create-react-app appName
  - npm start
  - npm install react-bootstrap bootstrap
*/

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// const name = 'John Smith';

// const user = {
//   firstName: 'Jane',
//   lastName: 'Smith'
// }

// function formatName(userArg) {
//   return `${userArg.firstName} ${userArg.lastName}`
// }

// const element = <h1>Hello {formatName(user)}</h1>

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//   element
// )
