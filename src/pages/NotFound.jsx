import { useNavigate } from "react-router-dom";

export default function NotFound(){
    const navigate = useNavigate()

    return(
        <>
            <h1>Error 404: Seems like you're lost</h1>
            <span>Back to <a href="/">homepage</a>.</span>
        </>
    )
}