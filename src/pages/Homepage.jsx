import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

export default function Homepage(){
    return(
        <>
            <Banner/>
            <Highlights/>
        </>
    )
}
