import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { useNavigate } from 'react-router-dom';
import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function AppNavbar({user, setUser}){

    console.log(user);

    const navigate = useNavigate();

    const { cUser, unsetCUser, setCUser } = useContext(UserContext);

    const logout = () => {
        localStorage.clear();
        unsetCUser();
        setCUser({id: null})
        setUser(null);
        Swal.fire('Logged out', '', 'info');
        navigate('/login');
    }

    return(
        <Navbar bg="light" expand="lg">
            <Container fluid>
                <Navbar.Brand onClick={() => navigate('/')}>Zuitt Booking (from AppNavbar.js)</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                    <Nav.Link onClick={() => navigate('/')}>Home</Nav.Link>
                    <Nav.Link onClick={() => navigate('/courses')}>Courses</Nav.Link>

                    {cUser.id === null ?
                        <>
                            <Nav.Link onClick={() => navigate('/login')}>Log In</Nav.Link>
                            <Nav.Link onClick={() => navigate('/register')}>Register</Nav.Link>
                        </> :
                        <Nav.Link onClick={logout}>Log Out {cUser.id}</Nav.Link>
                    }

                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}